<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
session_start();
class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
        var $rules_admin = array();
	var $rules_user = array('main','facebook');
	var $rules_guest = array('cruds/usuarios');
	private $restricted = 'conectar';
	var $errorView = '404';
	var $pathPictures = 'assets/uploads/pictures';
        var $pathAvatars = 'assets/uploads/pictures';
        var $pathCurriculos = 'assets/uploads/pictures';
        
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
                $this->load->helper('html');
                $this->load->helper('h');
                $this->load->model('user');
                $this->load->library('grocery_crud');
                $this->load->library('notificaciones');
                $this->load->database();
                $this->load->library('nexmo');
                $this->load->library('mailer');
                $this->nexmo->set_format('json');
	}
        
        public function index()
	{
            $this->loadView('main');
	}
        
        public function success($msj)
	{
		return '<div class="alert alert-success">'.$msj.'</div>';
	}

	public function error($msj)
	{
		return '<div class="alert alert-danger">'.$msj.'</div>';
	}
        
        public function login()
	{
		if(!$this->user->log)
		{	
			if(!empty($_POST['usuario']) && !empty($_POST['pass']))
			{
				$this->db->where('username',$this->input->post('usuario'));
				$r = $this->db->get('user');
				if($this->user->login($this->input->post('usuario',TRUE),$this->input->post('pass',TRUE)))
				{
					if($r->num_rows>0 && $r->row()->status==1)
					{
                                            if(empty($_POST['redirect']))
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.site_url().'"</script>');
                                            else
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.$_POST['redirect'].'"</script>');
					}
					else $this->loadView(array('view'=>'main','msj'=>$this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema')));
				}
                                else $this->loadView(array('view'=>'main','msj'=>$this->error('Usuario o contrasena incorrecta, intente de nuevo.')));
			}
			else
                            $this->loadView(array('view'=>'main','msj'=>$this->error('Debe completar todos los campos antes de continuar')));
		}
	}

	public function unlog()
	{
		$this->user->unlog();
                header("Location:".site_url());
	}
        
        public function loadView($param = array('view'=>'main'))
        {
            if(is_string($param))
            $param = array('view'=>$param);
            $this->load->view('template',$param);
        }
		
	public function loadViewAjax($view,$data = null)
        {
            $view = $this->valid_rules($view);
            $this->load->view($view,$data);
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */