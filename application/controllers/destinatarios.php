<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Destinatarios extends Panel {
        
	public function __construct()
	{
            parent::__construct();
            if($_SESSION['cuenta']!=1 && $_SESSION['cuenta']!=3)
                header("Location:".base_url('panel'));
            if($this->db->get_where('destinatarios',array('user'=>$_SESSION['user']))->num_rows==0)
                header("Location:".base_url('panel/destinatarios/add'));
	}
        
        public function index($url = 'main',$page = 0)
	{
            parent::index();
	}
        /*Cruds*/
        function expedientes($var = '',$x = '')
        {
            if($var=='read')
            $this->db->update('expedientes',array('fecha_recepcion'=>date("Y-m-d H:i:s"),'visto'=>1),array('destinatario'=>$this->db->get_where('destinatarios',array('user'=>$_SESSION['user']))->row()->id,'id'=>$x));
            parent::expedientes();
            $this->crud->where('expedientes.destinatario',$this->db->get_where('destinatarios',array('user'=>$_SESSION['user']))->row()->id);
            $this->crud->or_where('providencias.destinatario',$this->db->get_where('destinatarios',array('user'=>$_SESSION['user']))->row()->id);
            $this->crud->add_action('<i class="glyphicon glyphicon-book"></i>','',base_url('destinatarios/prov').'/','');
            $this->crud->display_as('nro_expediente','nro. Acta');
            $output = $this->crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
        function expedientes_enviados($var = '',$x = '')
        {
            if($var=='read')
            $this->db->update('expedientes',array('fecha_recepcion'=>date("Y-m-d H:i:s"),'visto'=>1),array('destinatario'=>$this->db->get_where('destinatarios',array('user'=>$_SESSION['user']))->row()->id,'id'=>$x));
            parent::expedientes();
            $this->crud->where('expedientes.remitente',$this->db->get_where('remitentes',array('user'=>$_SESSION['user']))->row()->id);
            $this->crud->or_where('providencias.destinatario',$this->db->get_where('destinatarios',array('user'=>$_SESSION['user']))->row()->id);
            $this->crud->add_action('<i class="glyphicon glyphicon-book"></i>','',base_url('destinatarios/prov').'/','');
            $output = $this->crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
        function prov($var = '')
        {
            if(!empty($var)){
            $p = $this->db->get_where('expedientes',array('id'=>$var));
            if($p->num_rows==0)
                $this->loadView('404');
            else
            {
                $_SESSION['expediente'] = $var;
                header("Location:".base_url('destinatarios/providencias/add'));
            }
            }
            else $this->loadView('404');
        }
        
        function provlist($var = '')
        {
            if(!empty($var)){
            $p = $this->db->get_where('expedientes',array('id'=>$var));
            if($p->num_rows==0)
                $this->loadView('404');
            else
            {
                $_SESSION['expediente'] = $var;
                header("Location:".base_url('destinatarios/providencias'));
            }
            }
            else $this->loadView('404');
        }
        
        function providencias()
        {
            parent::providencias();
            $output = $this->crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        /*Callbacks*/
        function expedientes_providencias($val,$row)
        {
            $p = $this->db->get_where('providencias',array('expediente'=>$row->id));
            return $p->num_rows>0?'<a href="'.base_url('destinatarios/provlist/'.$row->id).'">'.$p->num_rows.' <i class="glyphicon glyphicon-search"></i></a>':(string)$p->num_rows;
        }
}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */