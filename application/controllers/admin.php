<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Admin extends Panel {
        
	public function __construct()
	{
            parent::__construct();
            if($_SESSION['cuenta']!=3)
                header("Location:".base_url('panel'));
	}
        
        public function index($url = 'main',$page = 0)
	{
            parent::index();
	}
        /*Cruds*/
        public function user($x = '', $y = ''){
            parent::user();            
            if(!empty($_POST) && !empty($y) && $this->db->get_where('user',array('id'=>$y))->row()->email != $_POST['email'])
            $this->crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
            $output = $this->crud->render();
            $output->view = 'panel';
            $output->crud = 'registro';
            $this->loadView($output);
        }
        
        public function paises(){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('paises');
            $crud->set_subject('Usuarios');
            //Fields
            
            //unsets
             $crud->unset_delete();
            //Displays
            //Fields types
            //Validations
            $crud->required_fields('nombre');
            //Callbacks
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
        public function ciudades(){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('ciudades');
            $crud->set_subject('Ciudades');
            $crud->set_relation('pais','paises','nombre');
            //Fields
            
            //unsets
             $crud->unset_delete();
            //Displays
            //Fields types
            //Validations
            $crud->required_fields('pais','nombre');
            //Callbacks
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
         public function remitentes(){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('remitentes');
            $crud->set_subject('Remitentes');
            $crud->set_relation('tipo','tipos_remitentes','nombre');
            $crud->set_relation('user','user','nombre');
            //Fields
            
            //unsets
             $crud->unset_delete();
            //Displays
            //Fields types
            $crud->field_type('creado','invisible');
            $crud->field_type('modificado','invisible');
            //Validations
            $crud->required_fields('user','tipo','institucion');
            $crud->callback_field('user',array($this,'remitentes_userField'));
            //Callbacks
            $crud->callback_before_insert(array($this,'addfecha'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
        public function destinatarios(){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('destinatarios');
            $crud->set_subject('Destinatarios');
            $crud->set_relation('user','user','nombre');
            $crud->set_relation('dependencia','dependencias','nombre');
            $crud->set_relation('cargo','cargos','nombre');
            $crud->set_relation('tipo','tipos_destinatarios','nombre');
            //Fields
            
            //unsets
             $crud->unset_delete();
            //Displays
            //Fields types
            $crud->field_type('creado','invisible');
            $crud->field_type('modificado','invisible');
            //Validations
            $crud->required_fields('user','tipo','dependencia','cargo');
            //Callbacks
            $crud->callback_field('user',array($this,'destinatarios_userField'));
            $crud->callback_before_insert(array($this,'addfecha'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
        public function dependencias(){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('dependencias');
            $crud->set_subject('Dependencia');
            //Fields
            
            //unsets
             $crud->unset_delete();
            //Displays
            //Fields types
            //Validations
            $crud->required_fields('nombre');
            //Callbacks
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
        public function cargos(){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('cargos');
            $crud->set_subject('Cargo');
            //Fields
            
            //unsets
             $crud->unset_delete();
            //Displays
            //Fields types
            //Validations
            $crud->required_fields('nombre');
            //Callbacks
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
        public function tipos_documento(){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('documentos');
            $crud->set_subject('Documentos');
            //Fields
            
            //unsets
             $crud->unset_delete();
            //Displays
            //Fields types
            //Validations
            $crud->required_fields('nombre');
            //Callbacks
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
        public function tipos_documentos_expedientes(){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('documentos_expedientes');
            $crud->set_subject('Documentos');
            //Fields
            
            //unsets
             $crud->unset_delete();
            //Displays
            //Fields types
            //Validations
            $crud->required_fields('nombre');
            //Callbacks
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
         public function tipos_destinatarios(){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('tipos_destinatarios');
            $crud->set_subject('Tipo de destinatario');
            //Fields
            
            //unsets
             $crud->unset_delete();
            //Displays
            //Fields types
            //Validations
            $crud->required_fields('nombre');
            //Callbacks
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
         public function tipos_remitentes(){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('tipos_remitentes');
            $crud->set_subject('Tipo de remitentes');
            //Fields
            
            //unsets
             $crud->unset_delete();
            //Displays
            //Fields types
            //Validations
            $crud->required_fields('nombre');
            //Callbacks
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
         public function providencias(){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('providencias');
            $crud->set_subject('Providencias');
            //Fields
            
            //unsets
             $crud->unset_delete();
            //Displays
            //Fields types
            //Validations
            $crud->required_fields('nombre');
            //Callbacks
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
         public function ajustes(){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('ajustes');
            $crud->set_subject('Ajustes');
            //Fields
            
            //unsets
             $crud->unset_delete()
                  ->unset_add();
            //Displays
            $crud->display_as('sms','Envio de sms');
            //Fields types
            //Validations
            $crud->required_fields('sms');
            //Callbacks
            $crud->callback_column('sms',array($this,'ajustes_sms'));
            $crud->callback_field('sms',array($this,'ajustes_smsField'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        /* Callbacks */
        function ajustes_sms($val)
        {
            return $val==0?'NO':'SI';
        }
        function ajustes_smsField($val)
        {
            return form_dropdown('sms',array('0'=>$this->ajustes_sms(0),'1'=>$this->ajustes_sms(1)),$val,'id="field-sms"');
        }
        function remitentes_userField($var = '')
        {
            $u = $this->db->query('select user.id,user.nombre from user left join remitentes on remitentes.user = user.id where remitentes.id IS NULL');
            $data = array();
            foreach($u->result() as $u)
            {
                $data[$u->id] = $u->nombre;
            }
            return form_dropdown('user',$data,$var,'id="field-user"');
        }
        
        function destinatarios_userField($var = '')
        {
            $u = $this->db->query('select user.id,user.nombre from user left join destinatarios on destinatarios.user = user.id where destinatarios.id IS NULL');
            $data = array();
            foreach($u->result() as $u)
            {
                $data[$u->id] = $u->nombre;
            }
            return form_dropdown('user',$data,$var,'id="field-user"');
        }
}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */