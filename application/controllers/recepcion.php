<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Recepcion extends Panel {
        
	public function __construct()
	{
            parent::__construct();
            if($_SESSION['cuenta']!=2 && $_SESSION['cuenta']!=3)
                header("Location:".base_url('panel'));
	}
        
        public function index($url = 'main',$page = 0)
	{
            parent::index();
	}
        /*Cruds*/
        function expedientes($var = '',$x = '')
        {
            parent::expedientes();
            $this->crud->callback_field('remitente',array($this,'expedientes_remitente'));
            $this->crud->add_action('<i class="glyphicon glyphicon-book" title="Providencias"></i>','',base_url('recepcion/prov').'/','');
            $this->crud->add_action('<i class="glyphicon glyphicon-remove-circle" title="Anular"></i>','',base_url('recepcion/anularExpediente').'/','');
            $this->crud->field_type('remitente','text');            
            $this->crud->field_type('nro_expediente','visible');
            $this->crud->where('visto >= ',0);
            $this->crud->set_rules('nro_expediente','Numero de expediente','required|integer');
            $output = $this->crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);   
        }
        
        function prov($var = '')
        {
            if(!empty($var)){
            $p = $this->db->get_where('expedientes',array('id'=>$var));
            if($p->num_rows==0)
                $this->loadView('404');
            else
            {
                $_SESSION['expediente'] = $var;
                header("Location:".base_url('recepcion/providencias/add'));
            }
            }
            else $this->loadView('404');
        }
        
        function provlist($var = '')
        {
            if(!empty($var)){
            $p = $this->db->get_where('expedientes',array('id'=>$var));
            if($p->num_rows==0)
                $this->loadView('404');
            else
            {
                $_SESSION['expediente'] = $var;
                header("Location:".base_url('recepcion/providencias'));
            }
            }
            else $this->loadView('404');
        }
        
        function providencias()
        {
            parent::providencias();
            $this->crud->where('expediente >',0);
            $this->crud->add_action('<i class="glyphicon glyphicon-remove-circle" title="Anular"></i>','',base_url('recepcion/anularProv').'/','');
            $output = $this->crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
        function usuarios($var = '',$x = '')
        {
            parent::user();
            $this->crud->field_type('rol','invisible');
            $this->crud->field_type('status','invisible');
            $this->crud->unset_back_to_list();
            $this->crud->required_fields('username','nombre','apellido','usuario','email','password','tipo_documento','nro_documento','fecha_nacimiento','lugar_nacimiento','pais','ciudad','direccion','celular','telefono');
            if(!empty($_POST) && !empty($_POST['username']))
            $this->crud->set_lang_string('insert_success_message','<script>document.location.href="'.base_url('recepcion/remitente/add/'.$_POST['username']).'"</script>');
            $output = $this->crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);   
        }
        
        function remitente($var = '',$x = '')
        {
            parent::remitentes();
            $this->crud->field_type('user','hidden',$this->db->get_where('user',array('username'=>$x))->row()->id);
            $this->crud->set_rules('user','Usuario','is_unique[remitentes.user]');
            //$this->crud->set_relation('user','user','username');
            $output = $this->crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);   
        }
        
        /*Callbacks*/
        function expedientes_remitente($post = ''){
            $this->db->select('remitentes.id as idest,user.*');
            $this->db->join('user','user.id = remitentes.user','inner');
            $d = $this->db->get('remitentes');
            $data = array();
            foreach($d->result() as $x)
            $data[$x->idest] = $x->nombre.' '.$x->apellido;
            return form_dropdown('remitente',$data,$post,'id="field-remitente" class="chosen-select"');
        }
        
        function expedientes_providencias($val,$row)
        {
            $p = $this->db->get_where('providencias',array('expediente'=>$row->id));
            return $p->num_rows>0?'<a href="'.base_url('recepcion/provlist/'.$row->id).'">'.$p->num_rows.' <i class="glyphicon glyphicon-search"></i></a>':(string)$p->num_rows;
        }
        
        function user_binsertion($post)
        {
            $post['creador'] = 'Registro manual';
            $post['fecha_registro'] = date("Y-m-d H:i:s");
            $post['fecha_modificacion'] = date("Y-m-d H:i:s");
            $this->pass = $post['password'];
            $post['password'] = md5($post['password']);
            $post['status'] = 1;
            return $post;
        }
        
        function anularExpediente($exp)
        {
            if(!empty($exp)){
                $_POST['exp'] = $exp;
                $this->form_validation->set_rules('exp','Expediente','required|integer|greather_than[0]');
                if($this->form_validation->run()){
                    $this->db->update('expedientes',array('visto'=>-1),array('id'=>$_POST['exp']));
                    header("Location:".base_url('recepcion/expedientes'));
                }
                else echo $this->error('ocurrió un error a la hora de anular.');
            }
            else
                echo $this->error('ocurrió un error a la hora de anular.');
        }
        
        function anularProv($prov)
        {
            if(!empty($prov)){
                $_POST['exp'] = $prov;
                $this->form_validation->set_rules('exp','Provicencia','required|integer|greather_than[0]');
                if($this->form_validation->run()){
                    $this->db->update('providencias',array('expediente'=>-1),array('id'=>$_POST['exp']));
                    header("Location:".base_url('recepcion/providencias'));
                }
                else echo $this->error('ocurrió un error a la hora de anular.');
            }
            else
                echo $this->error('ocurrió un error a la hora de anular.');
        }
}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */