<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Panel extends Main {
        
	public function __construct()
	{
		parent::__construct();
                if(empty($_SESSION['user']))
                header("Location:".base_url());
	}
       
        public function index($url = 'main',$page = 0)
	{
		$this->loadView('inicio');
	}
        
        public function loadView($crud)
        {
            if(empty($_SESSION['user']))
            header("Location:".base_url());
            else
            parent::loadView($crud);
        }
        
        function redirect()
        {
            header("Location:".base_url('panel'));
            exit();
        }
        
        function addfecha($post)
        {
            $post['creado'] = date("Y-m-d H:i:s");
            return $post;
        }
        /*Cruds*/
        
        public function user($x='',$y=''){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('user');
            $crud->set_subject('Usuarios');
            $crud->set_relation('tipo_documento','documentos','nombre');
            $crud->set_relation('pais','paises','nombre');
            $crud->set_relation('ciudad','ciudades','nombre');
            //Fields
            
            //unsets
            $crud->unset_delete();
            //Displays
            $crud->display_as('username','Usuario')
                 ->display_as('password','Contraseña')
                 ->display_as('tipo_documento','Tipo de documento')
                 ->display_as('nro_documento','Numero de documento');
            $crud->columns('nombre','apellido','username','email','status','rol');
            //Fields types
            $crud->field_type('creador','invisible')
                 ->field_type('fecha_registro','invisible')
                 ->field_type('fecha_modificacion','invisible')
                 ->field_type('password','password');
            $crud->edit_fields('status','email','rol','nombre','apellido','password','tipo_documento','nro_documento','fecha_nacimiento','lugar_nacimiento','pais','ciudad','direccion','celular','telefono');
            //Validations
            $crud->required_fields('username','email','status','rol','nombre','apellido','usuario','email','password','tipo_documento','nro_documento','fecha_nacimiento','lugar_nacimiento','pais','ciudad','direccion','celular','telefono');            
            $crud->set_rules('password','Contraseña','required|min_length[8]|max_length[255]')
                 ->set_rules('username','Usuario','required|is_unique[user.username]');            
            //Callbacks
            $crud->callback_before_insert(array($this,'user_binsertion'));
            $crud->callback_after_insert(array($this,'user_ainsertion'));
            $crud->callback_before_update(array($this,'user_bupdate'));
            $crud->callback_column('status',array($this,'user_status'));
            $crud->callback_column('rol',array($this,'user_rol'));
            $crud->callback_field('status',array($this,'user_statusField'));
            $crud->callback_field('rol',array($this,'user_rolField'));
            $this->crud = $crud;
        }
        function expedientes()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('expedientes');
            $crud->set_subject('expediente');
            $crud->set_relation('documento','documentos_expedientes','nombre');
            $crud->set_model('expedientes');
            //Fields
            
            //unsets
            $crud->unset_columns('archivo','creado','modificado');
            $crud->columns('id','nro_expediente','fecha','fecha_recepcion','remitente','nro_documento','destinatario','motivo','visto','providencias');
            $crud->unset_delete();
            //Displays
            $crud->set_lang_string('form_save','Enviar');
            $crud->set_lang_string('form_save_and_go_back','Enviar y volver los expedientes');
            $crud->display_as('nro_expediente','Nro. Acta');
            //Fields types
            $crud->field_type('creado','invisible');
            $crud->field_type('modificado','invisible');
            $crud->field_type('remitente','invisible');
            $crud->field_type('fecha_recepcion','invisible');
            $crud->field_type('nro_expediente','invisible');
            $crud->field_type('visto','invisible');
            $crud->set_field_upload('archivo','files');
            //Validations
            $crud->required_fields('documento','destinatario','motivo');
            //Callbacks
            $crud->callback_before_insert(array($this,'expedientes_binsertion'));
            $crud->callback_after_insert(array($this,'expedientes_ainsertion'));
            $crud->callback_field('destinatario',array($this,'expedientes_destinatarioField'));
            $crud->callback_column('destinatario',array($this,'expedientes_destinatario'));
            $crud->callback_column('remitente',array($this,'expedientes_remitentes'));
            $crud->callback_column('visto',array($this,'expedientes_visto'));
            $crud->callback_column('providencias',array($this,'expedientes_providencias'));
            $this->crud = $crud;
        }
        
        function remitentes()
        {
            if((($_SESSION['cuenta'] == 0 || $_SESSION['cuenta'] == 3) && $this->db->get_where('remitentes',array('user'=>$_SESSION['user']))->num_rows==0) || ($_SESSION['cuenta'] == 2 || $_SESSION['cuenta'] == 3)){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('remitentes');
            $crud->set_relation('tipo','tipos_remitentes','nombre');
            //Fields
            
            //unsets
            $crud->unset_back_to_list()
                 ->unset_edit()
                 ->unset_list()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->unset_delete();
            //Displays
            $crud->set_lang_string('insert_success_message','<script>document.location.href="'.base_url('remitentes/expedientes').'"</script>');
            //Fields types
            $crud->field_type('creado','invisible');
            $crud->field_type('modificado','invisible');
            //Validations
            $crud->required_fields('tipo');
            //Callbacks
            $crud->callback_before_insert(array($this,'remitentes_binsertion'));
            $this->crud = $crud;
            }
            else
            header("Location:".base_url('panel'));
        }
        
        function destinatarios()
        {
            if(($_SESSION['cuenta'] == 1 || $_SESSION['cuenta'] == 3) && $this->db->get_where('destinatarios',array('user'=>$_SESSION['user']))->num_rows==0){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('destinatarios');
            $crud->set_relation('dependencia','dependencias','nombre');
            $crud->set_relation('cargo','cargos','nombre');
            $crud->set_relation('tipo','tipos_destinatarios','nombre');
            //Fields
            
            //unsets
            $crud->unset_back_to_list()
                 ->unset_edit()
                 ->unset_list()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->unset_delete();
            //Displays
            $crud->set_lang_string('insert_success_message','<script>document.location.href="'.base_url('destinatarios/expedientes').'"</script>');
            //Fields types
            $crud->field_type('user','invisible');
            $crud->field_type('creado','invisible');
            $crud->field_type('modificado','invisible');
            //Validations
            $crud->required_fields('tipo','dependencia','cargo');
            //Callbacks
            $crud->callback_before_insert(array($this,'remitentes_binsertion'));
            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
            }
            else
            header("Location:".base_url('panel'));
        }
        
        function providencias()
        {
           if(!empty($_SESSION['expediente'])){
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('providencias');
            $crud->set_subject('providencia');
            $crud->where('expediente',$_SESSION['expediente']);
            //Fields
            $crud->columns('fecha','contestacion');
            //unsets
            $crud->unset_delete();
            //Displays 
            $crud->display_as('destinatario','Derivar a');           
            //Fields types
            $crud->field_type('expediente','invisible');
            $crud->field_type('origen','invisible');
            $crud->field_type('creado','invisible');
            $crud->field_type('modificado','invisible');
            $crud->set_field_upload('archivo','files');
            $crud->callback_field('destinatario',array($this,'providencias_destinatarioField'));
            //Validations
            $crud->required_fields('fecha','contestacion','destinatario');
            //Callbacks
            $crud->callback_before_insert(array($this,'providencia_binsertion'));
            $crud->callback_after_insert(array($this,'providencia_ainsertion'));
            $this->crud = $crud;
            }
            else
            header("Location:".base_url('panel'));
        }
       
        /* Callbacks */
        function expedientes_binsertion($post)
        {
            $post['fecha'] = date("Y-m-d H:i:s");
            $post = $this->addfecha($post);
            $post['remitente'] = empty($post['remitente'])?$this->db->get_where('remitentes',array('user'=>$_SESSION['user']))->row()->id:$post['remitente'];
            
            return $post;
        }
        
        function expedientes_ainsertion($post,$id)
        {
            $this->notificaciones->notificar_expediente($id);
            return true;
        }
        
        function expedientes_destinatarioField($post = '')
        {
            $this->db->select('destinatarios.id as idest,user.*, dependencias.nombre as dependencia');
            $this->db->join('user','user.id = destinatarios.user','inner');
            $this->db->join('dependencias','destinatarios.dependencia = dependencias.id','inner');
            $d = $this->db->get('destinatarios');
            $data = array();
            foreach($d->result() as $x)
            $data[$x->idest] = $x->nombre.' '.$x->apellido.' ('.$x->dependencia.')';
            return form_dropdown('destinatario',$data,$post,'id="field-destinatario" class="chosen-select"');
        }
        
        function providencias_destinatarioField($post = '')
        {
            $this->db->select('destinatarios.id as idest,user.*, dependencias.nombre as dependencia');
            $this->db->join('user','user.id = destinatarios.user','inner');
            $this->db->join('dependencias','destinatarios.dependencia = dependencias.id','inner');
            $d = $this->db->get('destinatarios');
            $data = array(0=>'Sin derivar');
            foreach($d->result() as $x)
            $data[$x->idest] = $x->nombre.' '.$x->apellido.' ('.$x->dependencia.')';
            return form_dropdown('destinatario',$data,$post,'id="field-destinatario" class="chosen-select"');
        }
        
        function expedientes_destinatario($post)
        {
            $this->db->select('destinatarios.id as idest, user.*');
            $this->db->join('user','user.id = destinatarios.user','inner');
            $d = $this->db->get_where('destinatarios',array('destinatarios.id'=>$post));
            return $d->row()->nombre;
        }
        
        function expedientes_remitentes($post)
        {
            $this->db->select('remitentes.id as idest,user.*');
            $this->db->join('user','user.id = remitentes.user','inner');
            $d = $this->db->get_where('remitentes',array('remitentes.id'=>$post));
            return $d->num_rows>0?$d->row()->nombre:$post;
        }
        
        function remitentes_binsertion($post)
        {
            $post['user'] = empty($post['user'])?$_SESSION['user']:$post['user'];
            $post['creado'] = date("Y-m-d H:i:s");
            $post['modificado'] = date("Y-m-d H:i:s");
            return $post;
        }
        
        function expedientes_visto($val)
        {
            return $val==1?'Revisado':'No visto';
        }
        
        function user_binsertion($post)
        {
            $post['creador'] = 'Registro manual';
            $post['fecha_registro'] = date("Y-m-d H:i:s");
            $post['fecha_modificacion'] = date("Y-m-d H:i:s");
            $this->pass = $post['password'];
            $post['password'] = md5($post['password']);
            return $post;
        }
        
        function user_bupdate($post,$id)
        {
            if($this->db->get_where('user',array('id'=>$id))->row()->password!=$post['password'])
            $post['password'] = md5($post['password']);
            return $post;
        }
        
        function user_ainsertion($post,$id)
        {
            $this->notificaciones->notificar_registro($id,$this->pass);
            return true;
        }
        
        function user_status($var)
        {
            return status($var);
        }
        
        function user_statusField($var = '')
        {
            return form_dropdown('status',array('0'=>status(0),'1'=>status(1)),$var,'id="field-status"');
        }
        
        function user_rol($var)
        {
            return rol($var);
        }
        
        function user_rolField($var)
        {
            return form_dropdown('rol',array('0'=>rol(0),'1'=>rol(1),'2'=>rol(2),'3'=>rol(3)),$var,'id="field-rol"');
        }
        
        function providencia_binsertion($post)
        {
            $post['expediente'] = $_SESSION['expediente'];
            $post['creado'] = date("Y-m-d H:i:s");
            $post['modificado'] = date("Y-m-d H:i:s");
            return $post;
        }
        
        function providencia_ainsertion($post,$id)
        {
            $this->notificaciones->notificar_providencia($id);
            return true;
        }
        
        function notificar($x = '')
        {
            if(!empty($x) && $this->db->get_where('expedientes',array('id'=>$x))->num_rows>0)
            {
                $expediente = $this->db->get_where('expedientes',array('id'=>$x))->row();
                $this->db->select('user.*');
                $this->db->join('user','user.id = destinatarios.user');
                $destinatario = $this->db->get_where('destinatarios',array('destinatarios.id'=>$expediente->destinatario))->row();
                correo($destinatario->email,'Te han enviado un expediente',$expediente->motivo);
            }
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
