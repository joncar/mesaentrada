<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Registro extends Main {
        
	public function __construct()
	{
            parent::__construct();
            if(!empty($_SESSION['user']))
                header("Location:".base_url('panel'));
	}
        
        public function index($url = 'main',$page = 0)
	{
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('user');
            $crud->set_subject('Usuarios');
            $crud->set_relation('tipo_documento','documentos','nombre');
            $crud->set_relation('pais','paises','nombre');
            $crud->set_relation('ciudad','ciudades','nombre');
            //Fields
            
            //unsets
            $crud->unset_back_to_list()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_edit()
                 ->unset_list()
                 ->unset_export()
                 ->unset_print();
            //Displays
            $crud->display_as('username','Usuario')
                 ->display_as('password','Contraseña')
                 ->display_as('tipo_documento','Tipo de documento')
                 ->display_as('nro_documento','Numero de documento');
            $crud->set_lang_string('insert_success_message','<script>document.location.href="'.base_url('panel').'"</script>');
            //Fields types
            $crud->field_type('rol','invisible')
                 ->field_type('creador','invisible')
                 ->field_type('fecha_registro','invisible')
                 ->field_type('fecha_modificacion','invisible')
                 ->field_type('password','password')
                 ->field_type('status','invisible');
            //Validations
            $crud->required_fields('username','nombre','apellido','usuario','email','password','tipo_documento','nro_documento','fecha_nacimiento','lugar_nacimiento','pais','ciudad','direccion','celular');
            $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]')
                 ->set_rules('password','Contraseña','required|min_length[8]|max_length[255]')
                 ->set_rules('username','Usuario','required|is_unique[user.username]');
            
            //Callbacks
            $crud->callback_before_insert(array($this,'binsertion'));
            $crud->callback_after_insert(array($this,'ainsertion'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'registro';
            $this->loadView($output);
	}
        /* Callbacks */
        function binsertion($post)
        {
            $post['creador'] = 'Registrado por usuario';
            $post['rol'] = 0;
            $post['fecha_registro'] = date("Y-m-d H:i:s");
            $post['fecha_modificacion'] = date("Y-m-d H:i:s");
            $this->pass = $post['password'];
            $post['password'] = md5($post['password']);
            $post['status'] = 1;
            return $post;
        }
        
        function ainsertion($post,$id)
        {
            $this->notificaciones->notificar_registro($id,$this->pass);
            $this->user->login_short($id);
            return true;
        }
        
        function forget($key = '')
        {
            if(empty($_POST) && empty($key))
            $this->loadView('forget');
            else
            {
                if(empty($key)){
                if(empty($_SESSION['key'])){
                $this->form_validation->set_rules('email','Email','required|valid_email');
                if($this->form_validation->run())
                {
                    $user = $this->db->get_where('user',array('email'=>$this->input->post('email')));
                    if($user->num_rows>0){
                        $_SESSION['key'] = md5(rand(0,2048));
                        $_SESSION['email'] = $this->input->post('email');
                        $this->mailer->mail($this->input->post('email'),'reestablecimiento de contraseña',$this->load->view('email/forget',array('user'=>$user->row()),TRUE));
                        $_SESSION['msj'] = $this->success('Los pasos para la restauracion han sido enviados a su correo electronico');
                        header("Location:".base_url('registro/forget'));
                        //$this->loadView(array('view'=>'forget','msj'=>$this->success('Los pasos para la restauracion han sido enviados a su correo electronico')));
                    }
                    else
                    $this->loadView(array('view'=>'forget','msj'=>$this->error('El correo que desea restablecer no esta registrado.')));
                }
                else
                    $this->loadView(array('view'=>'forget','msj'=>$this->error($this->form_validation->error_string())));
                }
                else
                {
                    $this->form_validation->set_rules('email','Email','required|valid_email');
                    $this->form_validation->set_rules('pass','Password','required|min_length[8]');
                    $this->form_validation->set_rules('pass2','Password2','required|min_length[8]|matches[pass]');
                    $this->form_validation->set_rules('key','Llave','required');
                    if($this->form_validation->run())
                    {
                        if($this->input->post('key') == $_SESSION['key'])
                        {
                            $this->db->update('user',array('password'=>md5($this->input->post('pass'))),array('email'=>$_SESSION['email']));
                            session_unset();
                            $this->loadView(array('view'=>'forget','msj'=>$this->success('Se ha restablecido su contraseña')));
                        }
                        else
                            $this->loadView(array('view'=>'recover','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));
                    }
                    else{
                        if(empty($_POST['key'])){
                        $this->loadView(array('view'=>'forget','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));    
                        session_unset();
                        }
                        else
                        $this->loadView(array('view'=>'recover','key'=>$key,'msj'=>$this->error($this->form_validation->error_string())));
                    }
                }
                }
                else
                {
                    if(!empty($_SESSION['key']) && $key==$_SESSION['key'])
                    {
                        $this->loadView(array('view'=>'recover','key'=>$key));
                    }
                    else
                    $this->loadView(array('view'=>'forget','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));
                }
            }
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */