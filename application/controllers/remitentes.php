<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Remitentes extends Panel {
        
	public function __construct()
	{
            parent::__construct();
            if($_SESSION['cuenta']!=0 && $_SESSION['cuenta']!=3)
                header("Location:".base_url('panel'));
            if($this->router->fetch_method()!='remitente' && $this->db->get_where('remitentes',array('user'=>$_SESSION['user']))->num_rows==0)
                header("Location:".base_url('remitentes/remitente/add'));
	}
        
        public function index($url = 'main',$page = 0)
	{
            parent::index();
	}
        
        function remitente()
        {
            parent::remitentes();
            $this->crud->field_type('user','invisible');
            $output = $this->crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
        function provlist($var = '')
        {
            if(!empty($var)){
            $p = $this->db->get_where('expedientes',array('id'=>$var));
            if($p->num_rows==0)
                $this->loadView('404');
            else
            {
                $_SESSION['expediente'] = $var;
                header("Location:".base_url('remitentes/providencias'));
            }
            }
            else $this->loadView('404');
        }
        /*Cruds*/
        function expedientes()
        {
            parent::expedientes();
            $this->crud->where('remitente',$this->db->get_where('remitentes',array('user'=>$_SESSION['user']))->row()->id);
            $this->crud->columns('nro_expediente','fecha','fecha_recepcion','remitente','nro_documento','destinatario','motivo','visto','providencias');
            $output = $this->crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        /*Callbacks*/
        function providencias()
        {
            parent::providencias();
            $this->crud->unset_add();
            $this->crud->unset_edit();
            $output = $this->crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }
        function expedientes_providencias($val,$row)
        {
            $p = $this->db->get_where('providencias',array('expediente'=>$row->id));
            return $p->num_rows>0?'<a href="'.base_url('remitentes/provlist/'.$row->id).'">'.$p->num_rows.' <i class="glyphicon glyphicon-search"></i></a>':(string)$p->num_rows;
        }
}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */