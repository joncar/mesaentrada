<?php
/**
 * PHP grocery CRUD
 *
 * LICENSE
 *
 * Grocery CRUD is released with dual licensing, using the GPL v3 (license-gpl3.txt) and the MIT license (license-mit.txt).
 * You don't have to do anything special to choose one license or the other and you don't have to notify anyone which license you are using.
 * Please see the corresponding license file for details of these licenses.
 * You are free to use, modify and distribute this software, but all copyright information must remain.
 *
 * @package    	grocery CRUD
 * @copyright  	Copyright (c) 2010 through 2012, John Skoumbourdis
 * @license    	https://github.com/scoumbourdis/grocery-crud/blob/master/license-grocery-crud.txt
 * @version    	1.2
 * @author     	John Skoumbourdis <scoumbourdisj@gmail.com>
 */

// ------------------------------------------------------------------------

/**
 * Grocery CRUD Model
 *
 *
 * @package    	grocery CRUD
 * @author     	John Skoumbourdis <scoumbourdisj@gmail.com>
 * @version    	1.2
 * @link		http://www.grocerycrud.com/documentation
 */
class Expedientes  extends grocery_CRUD_Model  {

    function __construct()
    {
        parent::__construct();
    }

    function get_list()
    {
        $this->db->select('expedientes.id, providencias.destinatario as dest, expedientes.nro_expediente, expedientes.fecha, expedientes.fecha_recepcion, expedientes.remitente, expedientes.destinatario, expedientes.motivo, expedientes.visto, user.nro_documento');
        $this->db->join('documentos_expedientes','documentos_expedientes.id = expedientes.documento','left');
        $this->db->join('providencias','expedientes.id = providencias.expediente','left');
        $this->db->join('remitentes','remitentes.id = expedientes.remitente','left');
        $this->db->join('user','user.id = remitentes.user','left');
        $this->db->group_by('expedientes.id');
    	$results = $this->db->get('expedientes')->result();
    	return $results;
    }
    
    function get_total_results()
    {
        $this->db->select('expedientes.id, providencias.destinatario as dest, expedientes.nro_expediente, expedientes.fecha, expedientes.fecha_recepcion, expedientes.remitente, expedientes.destinatario, expedientes.motivo, expedientes.visto, user.nro_documento');
        $this->db->join('documentos_expedientes','documentos_expedientes.id = expedientes.documento','left');
        $this->db->join('providencias','expedientes.id = providencias.expediente','left');
        $this->db->join('remitentes','remitentes.id = expedientes.remitente','left');
        $this->db->join('user','user.id = remitentes.user','left');
        $this->db->group_by('expedientes.id');
    	return $this->db->get($this->table_name)->num_rows;

    }
    
    function like($field, $match = '', $side = 'both')
    {
        switch($field)
        {
            case 'id': 
            case 'fecha':
            case 'destinatario': $field = 'expedientes.'.$field; break;
            case 'remitente': 
                $this->db->like('nombre',$match);
                $this->db->or_like('apellido',$match);
                $user = $this->db->get('user');
                if($user->num_rows>0){
                $match = $this->db->get_where('remitentes',array('user'=>$user->row()->id))->row()->id;
                }
            break;
        }
    	if($field!='providencias')$this->db->or_like($field, $match, $side);
    }
    
    function or_like($field, $match = '', $side = 'both')
    {
        switch($field)
        {
            case 'id': 
            case 'fecha':
            case 'destinatario': $field = 'expedientes.'.$field; break;
        }
    	if($field!='providencias')$this->db->or_like($field, $match, $side);
    }
}
