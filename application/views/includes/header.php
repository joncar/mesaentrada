<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li class="active"><a href="<?= empty($_SESSION['user'])?site_url():base_url('panel') ?>">Inicio</a></li>
          <? if(isset($_SESSION['cuenta'])): ?>
          <? if($_SESSION['cuenta']==3): ?>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tablas <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="<?= base_url('admin/user') ?>">Usuarios</a></li>
              <li><a href="<?= base_url('admin/paises') ?>">Paises</a></li>
              <li><a href="<?= base_url('admin/ciudades') ?>">Ciudades</a></li>
              <li><a href="<?= base_url('admin/tipos_documento') ?>">Tipos de documentos</a></li>
              <li><a href="<?= base_url('admin/tipos_documentos_expedientes') ?>">Tip/doc en expedientes</a></li>
              <li><a href="<?= base_url('admin/remitentes') ?>">Remitentes</a></li>
              <li><a href="<?= base_url('admin/destinatarios') ?>">Destinatarios</a></li>
              <li><a href="<?= base_url('admin/dependencias') ?>">Dependencias</a></li>
              <li><a href="<?= base_url('admin/cargos') ?>">Cargos</a></li>
              <li><a href="<?= base_url('admin/tipos_destinatarios') ?>">Tipos de destinatarios</a></li>
              <li><a href="<?= base_url('admin/tipos_remitentes') ?>">Tipos de remitentes</a></li>
              <li><a href="<?= base_url('admin/providencias') ?>">Providencias</a></li>
              <li><a href="<?= base_url('admin/ajustes') ?>">Ajustes del sistema</a></li>
            </ul>
          </li>
          <? endif ?>
          <? if($_SESSION['cuenta']==0 || $_SESSION['cuenta'] == 3): ?>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Remitentes <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="<?= base_url('remitentes/expedientes') ?>">Expedientes</a></li>
            </ul>
          </li>
          <? endif ?>
          <? if($_SESSION['cuenta']==1 || $_SESSION['cuenta'] == 3): ?>
          <? $exp = $this->db->get_where('destinatarios',array('user'=>$_SESSION['user']))->num_rows>0?$this->db->get_where('expedientes',array('visto'=>'0','destinatario'=>$this->db->get_where('destinatarios',array('user'=>$_SESSION['user']))->row()->id))->num_rows:0 ?>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Destinatarios <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="<?= base_url('destinatarios/expedientes') ?>">Expedientes <?= $exp>0?'<span class="badge">'.$exp.'</span>':'' ?></a></li>
              <li><a href="<?= base_url('destinatarios/expedientes_enviados') ?>">Expedientes Enviados</a></li>
            </ul>
          </li>
          <? endif ?>
          <? if($_SESSION['cuenta']==2 || $_SESSION['cuenta'] == 3): ?>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Recepcion <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="<?= base_url('recepcion/usuarios') ?>">Usuarios</a></li>
              <li><a href="<?= base_url('recepcion/expedientes') ?>">Expedientes</a></li>
            </ul>
          </li>
          <? endif ?>
          <li><a href="<?= base_url('main/unlog') ?>">Salir</a></li>
          <? endif ?>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>