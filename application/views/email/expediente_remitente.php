    <section style='background:#f3f3f3; margin:0px; padding:10px;'>
    <header>
    <div style="display:block"><?= img('img/logo.png') ?></div>
    <div style="display:inline-block"><h1>Hola <?= $exp->remdata ?></h1></div>
    </header>
        <section style='background:white; padding:10px; border-radius:1em; margin:10px;'>
            <p>Hemos enviado tu expediente</p>
            <p>Nro. Expediente: <b><?= $exp->id ?></b></p>
            <p>Motivo: <b><?= strip_tags($exp->motivo) ?></b></p>
            <p>Destinatario: <b><?= $exp->destino ?></b></p>
            <p>Entra en este link <a href="http://www.fderecho.net/mesaentrada">www.fderecho.net/mesaentrada</a> para enviar o revisar la situación de tu expediente o documento.</p>
        </section>
        <footer style='text-align:center'>
        Copyrigth DTI – Dirección de Tecnología de la Facultad de Derecho, Ciencias Políticas y Sociales, UNP. Teléf.: 0786 230 051
    </footer>
    </section>