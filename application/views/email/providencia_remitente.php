    <section style='background:#f3f3f3; margin:0px; padding:10px;'>
    <header>
    <div style="display:block"><?= img('img/logo.png') ?></div>
    <div style="display:inline-block"><h1>Hola <?= $exp->remdata ?></h1></div>
    </header>
        <section style='background:white; padding:10px; border-radius:1em; margin:10px;'>
            <p>Tu expediente <b>Nº <?= $exp->id ?></b> ha sido providenciado en fecha <b><?= $exp->provfecha ?></b></p>
            <p>Providencia: <b><?= strip_tags($exp->providencia) ?></b></p>
            <p>Entra en este link www.fderecho.net/mesaentrada para revisar más detalles de las  providencias a tu expediente o llamar al 0786 230 051 para más consultas.</p>
        </section>
        <footer style='text-align:center'>
        Copyrigth DTI – Dirección de Tecnología de la Facultad de Derecho, Ciencias Políticas y Sociales, UNP. Teléf.: 0786 230 051
    </footer>
    </section>