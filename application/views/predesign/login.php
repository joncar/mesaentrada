<? if(empty($_SESSION['user'])): ?>
<? if(!empty($msj))echo $msj ?>
<form role="form" class="form-horizontal well" action="<?= base_url('main/login') ?>" onsubmit="return validar(this)" method="post">
   <?= input('usuario','Usuario','text') ?>
   <?= input('pass','Contraseña','password') ?>
   <input type="hidden" name="redirect" value="<?= base_url('panel') ?>">
   <div align="center"><button type="submit" class="btn btn-success">Ingresar</button>
   <a class="btn btn-link" href="<?= base_url('registro/index/add') ?>">Registrate</a><br/>
   <a class="btn btn-link" href="<?= base_url('registro/forget') ?>">¿Olvidó su contraseña?</a>
   </div>
</form>
<? else: ?>
<div align="center"><a href="<?= base_url('panel') ?>" class="btn btn-success btn-large">Entrar al sistema</a></div>
<? endif; ?>
