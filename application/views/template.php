<!Doctype html>
<html lang="es">
	<head>
		<title>Mesa entrada</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<script src="http://code.jquery.com/jquery-1.9.0.js"></script>
		<script src="<?= base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
		<script src="<?= base_url('assets/frame.js') ?>"></script>
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/style.css') ?>">
		<?php 
		if(!empty($css_files) && !empty($js_files)):
		foreach($css_files as $file): ?>
		<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
		<?php endforeach; ?>
		<?php foreach($js_files as $file): ?>
		<script src="<?= $file ?>"></script>
		<?php endforeach; endif; ?>
                
                <!-- Respond.js proxy on external server -->
	</head>
	<body>                
            <header>
                <nav>
                    <div class="container">
                        <div class="col-xs-10">Trabajando contigo por la ciudad que queremos</div>
                        <div class="col-xs-2" align="right"><?= !empty($_SESSION['user'])?$_SESSION['nombre'].' '.$_SESSION['nombre'].' | <a href="'.base_url('main/unlog').'">Salir</a>':'' ?></div>
                    </div>
                </nav>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6">
                            <a href="http://pilar.gov.py/" rel="home" title="Municipalidad de Pilar" id="logo">
                                <img src="http://pilar.gov.py/wp-content/uploads/2013/05/head2.png">
                            </a>
                        </div>
                        <div class="col-xs-6" align="right">
                            <a title="" id="logo2">
                                <img alt="" src="http://pilar.gov.py/wp-content/uploads/2013/06/header-01.png">
                            </a>
                        </div>
                    </div>                    
                </div>
            </header>
            <div class="row">
                        <div class="col-xs-12" align="right">
                            <?= $this->load->view('includes/header') ?>
                        </div>
            </div>
            <section class="container">                
                <?php $this->load->view($view); ?>
            </section>
        </body>
</html>